# How to run this project
* run **yarn install**
* open android emulator or ios simulator
* run **yarn run android** or **yarn run ios**

# How to add firebase
* Visit this [react-native-firebase](https://rnfirebase.io/docs/v5.x.x/installation/initial-setup) and complete following steps for initial setup.