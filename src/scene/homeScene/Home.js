import React from 'react'
import { View, Text } from 'react-native'
import { HomeContainer } from '../../feature'
const Home = () => {
  return(
    <View>
      <HomeContainer />
    </View>
  )
}

export default Home